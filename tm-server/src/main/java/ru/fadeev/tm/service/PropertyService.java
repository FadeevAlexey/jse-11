package ru.fadeev.tm.service;

import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.Application;
import ru.fadeev.tm.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    private final String configDir = "/config.properties";

    private final Properties properties = new Properties();

    @Override
    public void init() throws Exception {
        try (final InputStream inputStream = Application.class.getResourceAsStream(configDir)) {
            properties.load(inputStream);
        }
    }

    @Override
    @Nullable
    public String getServerPort() {
        return properties.getProperty("server.port");
    }

    @Override
    @Nullable
    public String getServerHost() {
        return properties.getProperty("server.host");
    }

    @Override
    @Nullable
    public String getSessionSalt() {
        return properties.getProperty("session.salt");
    }

    @Override
    public int getSessionCycle() {
        String cycle = properties.getProperty("session.cycle");
        if (cycle != null && cycle.matches("\\d+")) return Integer.parseInt(cycle);
        return 0;
    }

    @Override
    public int getSessionLifetime() {
        String cycle = properties.getProperty("session.lifetime");
        if (cycle != null && cycle.matches("\\d+")) return Integer.parseInt(cycle);
        return Integer.MAX_VALUE;
    }

}