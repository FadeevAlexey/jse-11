package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.repository.IProjectRepository;
import ru.fadeev.tm.api.service.IProjectService;
import ru.fadeev.tm.constant.Sort;
import ru.fadeev.tm.entity.Project;
import ru.fadeev.tm.exception.IllegalSortTypeException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    private final IProjectRepository projectRepository;

    public ProjectService(@NotNull final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Nullable
    @Override
    public String findIdByName(@Nullable final String name, @Nullable final String userId) {
        if (name == null || name.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return projectRepository.findIdByName(name, userId);
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return new ArrayList<>();
        return projectRepository.findAll(userId);
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        projectRepository.removeAll(userId);
    }

    @NotNull
    @Override
    public Collection<Project> sortAll(@Nullable final String userId, @Nullable final String sortRequest) {
        if (userId == null || userId.isEmpty()) return new ArrayList<>();
        if (sortRequest == null || sortRequest.isEmpty()) return findAll(userId);
        return projectRepository.findAll(userId, getComparator(sortRequest));
    }

    @Override
    @NotNull
    public Collection<Project> searchByName(@Nullable final String userId, @Nullable final String string) {
        if (userId == null || userId.isEmpty()) return new ArrayList<>();
        if (string == null || string.isEmpty()) return new ArrayList<>();
        return projectRepository.searchByName(userId, string);
    }

    @Override
    @NotNull
    public Collection<Project> searchByDescription(@Nullable final String userId, @Nullable final String string) {
        if (userId == null || userId.isEmpty()) return new ArrayList<>();
        if (string == null || string.isEmpty()) return new ArrayList<>();
        return projectRepository.searchByDescription(userId, string);
    }

    private Comparator<Project> getComparator(@Nullable String selectedSort){
        if (selectedSort == null || selectedSort.isEmpty()) return null;
        @NotNull Comparator sortOrder = Comparator.naturalOrder();
        if (selectedSort.toUpperCase().contains(Sort.SUFFIX)) {
            sortOrder = Comparator.reverseOrder();
            selectedSort = selectedSort.substring(0,selectedSort.length() - Sort.SUFFIX.length());
        }
        @NotNull Comparator sortOptions = Comparator.nullsLast(sortOrder);
        if (selectedSort.toUpperCase().contains(Sort.PREFIX)){
            sortOptions = Comparator.nullsFirst(sortOrder);
            selectedSort = selectedSort.substring(Sort.PREFIX.length());
        }
        switch (selectedSort.toLowerCase()){
            case "start date":
                return Comparator.comparing(Project::getStartDate, sortOptions);
            case "finish date":
                return Comparator.comparing(Project::getFinishDate,sortOptions);
            case "status":
                return Comparator.comparing(Project::getStatus, sortOptions);
            default:
                throw new IllegalSortTypeException("cannot sort by type :" +selectedSort);
        }
    }

}