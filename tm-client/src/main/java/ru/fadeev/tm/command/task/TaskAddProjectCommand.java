package ru.fadeev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.IProjectEndpoint;
import ru.fadeev.tm.api.endpoint.ITaskEndpoint;
import ru.fadeev.tm.api.endpoint.Session;
import ru.fadeev.tm.api.endpoint.Task;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.AccessDeniedException;
import ru.fadeev.tm.exception.IllegalTaskNameException;

public final class TaskAddProjectCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-addProject";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Adding task to the project.";
    }

    @Override
    public void execute() {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @NotNull final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        @Nullable final Session session = serviceLocator.getAppStateService().getSession();
        if (session == null) throw new AccessDeniedException("Access denied");
        terminal.println("[ADD TASK TO PROJECT]");
        terminal.println("ENTER TASK NAME");
        @Nullable final String taskId = taskEndpoint.findIdByNameTask(session,terminal.readString());
        terminal.println("ENTER PROJECT NAME");
        @Nullable final String projectId = projectEndpoint.findIdByNameProject(session,terminal.readString());
        if (taskId == null || projectId == null) throw new IllegalTaskNameException("Can't find project or task");
        @Nullable final Task task = taskEndpoint.findOneTask(session,taskId);
        if (task == null) throw new IllegalTaskNameException("Can't find project or task");
        task.setProjectId(projectId);
        taskEndpoint.mergeTask(session, task);
        terminal.println("[OK]\n");
    }

}