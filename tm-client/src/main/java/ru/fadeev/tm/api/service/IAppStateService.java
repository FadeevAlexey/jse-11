package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.Role;
import ru.fadeev.tm.api.endpoint.Session;
import ru.fadeev.tm.api.endpoint.User;
import ru.fadeev.tm.command.AbstractCommand;

import java.util.List;

public interface IAppStateService {

    @Nullable
    Role getRole();

    void putCommand(@Nullable String description, @Nullable AbstractCommand abstractCommand);

    @Nullable
    AbstractCommand getCommand(@Nullable String command);

    @NotNull
    List<AbstractCommand> getCommands();

    boolean hasPermission(@Nullable Role... roles);

    @Nullable
    Session getSession();

    void setSession(@Nullable Session session);

}
