package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectService extends IService<Project> {

    @Nullable
    String findIdByName(@Nullable String name, @Nullable String userId);

    @NotNull
    List<Project> findAll(@Nullable String userId);

    void removeAll(@Nullable String userId);

    @NotNull
    Collection<Project> sortAll(@NotNull String userId, @Nullable String sortRequest);

    @NotNull
    Collection<Project> searchByName(@Nullable String userId, @Nullable String string);

    @NotNull
    Collection<Project> searchByDescription(@Nullable String userId, @Nullable String string);

}