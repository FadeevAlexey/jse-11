package ru.fadeev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.Session;
import ru.fadeev.tm.api.endpoint.User;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.AccessDeniedException;
import ru.fadeev.tm.exception.IllegalUserPasswordException;
import ru.fadeev.tm.exception.InvalidSessionException;

public final class UserUpdatePasswordCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-updatePassword";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "User Password Changes.";
    }

    @Override
    public void execute() {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @Nullable final Session session = serviceLocator.getAppStateService().getSession();
        if (session == null) throw new AccessDeniedException("Access denied");
        @Nullable final User currentUser = serviceLocator.getUserEndpoint().findOneUser(session, session.getUserId());
        if (currentUser == null)
            throw new InvalidSessionException("You were logout. Please log in again");
        terminal.println("[UPDATE PASSWORD]");
        terminal.println("Enter your current password");
        @Nullable final String currentPassword = terminal.readString();
        if (currentPassword == null) throw new IllegalUserPasswordException("Illegal password");
        if (!currentPassword.equals(currentUser.getPassword()))
            throw new IllegalUserPasswordException("wrong password");
        terminal.println("Enter new password");
        @Nullable final String newPassword = terminal.readString();
        currentUser.setPassword(newPassword);
        serviceLocator.getUserEndpoint().mergeUser(session, currentUser);
        terminal.println("[OK]\n");
    }

}