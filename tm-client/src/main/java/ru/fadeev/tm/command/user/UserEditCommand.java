package ru.fadeev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.IUserEndpoint;
import ru.fadeev.tm.api.endpoint.Session;
import ru.fadeev.tm.api.endpoint.User;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.AccessDeniedException;
import ru.fadeev.tm.exception.IllegalUserNameException;

public final class UserEditCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit user profile";
    }

    @Override
    public void execute() {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @NotNull final IUserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
        @Nullable final Session session = serviceLocator.getAppStateService().getSession();
        if (session == null) throw new AccessDeniedException("Access denied");
        @Nullable final User currentUser = userEndpoint.findOneUser(session, session.getUserId());
        if (currentUser == null) throw new IllegalUserNameException("Can't find user");
        terminal.println("[EDIT PROFILE]");
        terminal.println("ENTER NEW NAME OR PRESS ENTER");
        @Nullable final String name = terminal.readString();
        if (userEndpoint.isLoginExistUser(name))
            throw new IllegalUserNameException("User with same name already exist");
        terminal.println("ENTER NEW PASSWORD OR PRESS ENTER");
        @Nullable final String newPassword = terminal.readString();
        if (newPassword != null && !newPassword.isEmpty()) currentUser.setPassword(newPassword);
        if (name != null && !name.isEmpty()) currentUser.setName(name);
        userEndpoint.mergeUser(session, currentUser);
        terminal.println("[OK]\n");
    }

}