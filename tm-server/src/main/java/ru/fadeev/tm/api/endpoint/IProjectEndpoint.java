package ru.fadeev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Project;
import ru.fadeev.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @NotNull
    @WebMethod
    List<Project> findAllProjectAdmin(@WebParam(name = "session") @Nullable Session session);

    @Nullable
    @WebMethod
    Project findOneProject(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "id") @Nullable String id
    );

    @Nullable
    @WebMethod
    Project removeProject(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "id") @Nullable String id
    );

    @Nullable
    @WebMethod
    Project persistProject(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "project") @Nullable Project project
    );

    @Nullable
    @WebMethod
    Project mergeProject(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "project") @Nullable Project project
    );

    @WebMethod
    void removeAllProjectAdmin(
            @WebParam(name = "session") @Nullable Session session
    );

    @Nullable
    @WebMethod
    String findIdByNameProject(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "name") @Nullable String name
    );

    @NotNull
    @WebMethod
    List<Project> findAllProject(@WebParam(name = "session") @Nullable Session session);

    @WebMethod
    void removeAllProject(
            @WebParam(name = "session") @Nullable Session session);

    @NotNull Collection<Project> sortAllProject(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "selectedSort") @Nullable String selectedSort
    );

    @NotNull
    @WebMethod
    Collection<Project> searchByNameProject(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "string") @Nullable String string
    );

    @NotNull
    @WebMethod
    Collection<Project> searchByDescriptionProject(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "string") @Nullable String string
    );

}