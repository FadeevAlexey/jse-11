package ru.fadeev.tm.endpoint;

import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.IUserEndpoint;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.Session;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Setter
@WebService(endpointInterface = "ru.fadeev.tm.api.endpoint.IUserEndpoint")
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(){
        super(null);
    }

    public UserEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public List<User> findAllUser(@WebParam(name = "session") @Nullable final Session session) {
        serviceLocator.getSessionService().checkSession(session, Role.ADMINISTRATOR);
        return serviceLocator.getUserService().findAll();
    }

    @Override
    @WebMethod
    public @Nullable User findOneUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) {
        serviceLocator.getSessionService().checkSession(session,Role.ADMINISTRATOR);
        return serviceLocator.getUserService().findOne(id);
    }

    @Override
    @WebMethod
    public @Nullable User removeUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) {
        serviceLocator.getSessionService().checkSession(session,Role.ADMINISTRATOR);
        return serviceLocator.getUserService().remove(id);
    }

    @Override
    @WebMethod
    public @Nullable User persistUser(
            @WebParam(name = "user") final @Nullable User user
    ) {
        if (user == null) return null;
        if (user.getRole() == Role.ADMINISTRATOR) return null;
        return serviceLocator.getUserService().persist(user);
    }

    @Override
    @WebMethod
    public @Nullable User mergeUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "user") final @Nullable User user
    ) {
        serviceLocator.getSessionService().checkSession(session);
        if (user == null) return null;
        if (user.getRole() == Role.ADMINISTRATOR && session.getRole() != Role.ADMINISTRATOR) return null;
        return serviceLocator.getUserService().merge(user);
    }

    @Override
    @WebMethod
    public void removeAllUser(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().checkSession(session, Role.ADMINISTRATOR);
        serviceLocator.getUserService().removeAll();
    }

    @Override
    @WebMethod
    public boolean isLoginExistUser(
            @WebParam(name = "login") @Nullable final String login) {
        return serviceLocator.getUserService().isLoginExist(login);
    }

    @Nullable
    @Override
    @WebMethod
    public User findUserByLoginUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login
    ) {
        serviceLocator.getSessionService().checkSession(session,Role.ADMINISTRATOR);
        return serviceLocator.getUserService().findUserByLogin(login);
    }

    @Override
    @WebMethod
    public void setAdminRole(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "user") @Nullable final User user) {
        serviceLocator.getSessionService().checkSession(session, Role.ADMINISTRATOR);
        serviceLocator.getUserService().setAdminRole(user);
    }

}