package ru.fadeev.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IDomainEndpoint {

    @WebMethod
    void saveToBinary(@WebParam(name = "session") @Nullable Session session) throws Exception;

    @WebMethod
    void loadFromBinary( @WebParam(name = "session") @Nullable Session session) throws Exception;

    @WebMethod
    void saveToJsonFasterXml(@WebParam(name = "session") @Nullable Session session) throws Exception;

    @WebMethod
    void loadFromJsonFasterXml(@WebParam(name = "session") @Nullable Session session) throws Exception;

    @WebMethod
    void saveToJsonJaxb(@WebParam(name = "session") @Nullable Session session) throws Exception;

    @WebMethod
    void loadFromJsonJaxb(@WebParam(name = "session") @Nullable Session session) throws Exception;

    @WebMethod
    void saveToXmlJaxb(@WebParam(name = "session") @Nullable Session session) throws Exception;

    @WebMethod
    void loadFromXmlJaxb(@WebParam(name = "session") @Nullable Session session) throws Exception;

    @WebMethod
    void saveToXmlFasterxml(@WebParam(name = "session") @Nullable Session session) throws Exception;

    @WebMethod
    void loadFromXmlFasterxml(@WebParam(name = "session") @Nullable Session session) throws Exception;

}
