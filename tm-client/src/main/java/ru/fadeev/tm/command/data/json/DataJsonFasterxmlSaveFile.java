package ru.fadeev.tm.command.data.json;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.Role;
import ru.fadeev.tm.api.endpoint.Session;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;

import java.nio.file.AccessDeniedException;

public final class DataJsonFasterxmlSaveFile extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "data-json-fasterxml-save";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "save data to json format file (FASTERXML)";
    }

    @Override
    public void execute() throws Exception{
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @Nullable final Session session = serviceLocator.getAppStateService().getSession();
        if (session == null) throw new AccessDeniedException("Access Denied");
        terminal.println("[SAVE DATA TO JSON FORMAT FILE]");
        serviceLocator.getDomainEndpoint().saveToJsonFasterXml(session);
        terminal.println("[OK]\n");
    }

    @Override
    public @Nullable Role[] accessRole() {
        return new Role[]{Role.ADMINISTRATOR};
    }

}