package ru.fadeev.tm.endpoint;

import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.ISessionEndpoint;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Setter
@WebService(endpointInterface = "ru.fadeev.tm.api.endpoint.ISessionEndpoint")
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    public SessionEndpoint(){
        super(null);
    }

    public SessionEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @Nullable
    @WebMethod
    public Session openSession(
            @WebParam(name = "session") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password) {
        return serviceLocator.getSessionService().openSession(login, password);
    }

    @Override
    @WebMethod
    public void closeSession(@WebParam(name = "session") @Nullable final Session session) {
        serviceLocator.getSessionService().closeSession(session);
    }

}