package ru.fadeev.tm.constant;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Data {

    public static final Path DIRECTORY = Paths.get(System.getProperty("user.dir") + File.separator + "data");

    public static final Path BINARY = Paths.get(DIRECTORY + File.separator + "data.bin");

    public static final Path JSON_FASTERXML = Paths.get(DIRECTORY + File.separator + "fasterxml.json");

    public static final Path JSON_JAXB = Paths.get(DIRECTORY + File.separator + "jaxb.json");

    public static final Path XML_JAXB = Paths.get(DIRECTORY + File.separator + "jaxb.xml");

    public static final Path XML_FASTERXML = Paths.get(DIRECTORY + File.separator + "fasterxml.xml");

}