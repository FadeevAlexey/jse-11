package ru.fadeev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.IProjectEndpoint;
import ru.fadeev.tm.api.endpoint.Session;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.constant.Sort;
import ru.fadeev.tm.exception.AccessDeniedException;

public final class ProjectSortListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-sortList";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Shows sorted projects by START DATE, FINISH DATE, STATUS.";
    }

    @Override
    public void execute() {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @Nullable final Session session = serviceLocator.getAppStateService().getSession();
        @Nullable final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        if (session == null || session.getUserId() == null) throw new AccessDeniedException("Access denied");
        terminal.println("[PROJECT SORT LIST]");
        terminal.println("Select sorting type:\n" +
                "START DATE, FINISH DATE, STATUS or press ENTER for default sort by adding\n" +
                "if you'd like descending sort use suffix "+ Sort.SUFFIX +", for example: STATUS" +
                Sort.SUFFIX +"\n" + "if you'd like show null first use prefix " + Sort.PREFIX +", for example: " +
                Sort.PREFIX + "START DATE or " + Sort.PREFIX + "START DATE"+ Sort.SUFFIX +" (only for date)");
        @Nullable final String sortRequest = terminal.readString();
        terminal.printProjectList(projectEndpoint.sortAllProject(session, sortRequest));
    }

}