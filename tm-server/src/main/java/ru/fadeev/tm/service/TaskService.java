package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ITaskService;
import ru.fadeev.tm.constant.Sort;
import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.api.repository.ITaskRepository;
import ru.fadeev.tm.exception.IllegalSortTypeException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    private final ITaskRepository taskRepository;

    public TaskService(@NotNull final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        taskRepository.removeAll(userId);
    }

    @NotNull
    @Override
    public Collection<Task> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return new ArrayList<>();
        return taskRepository.findAll(userId);
    }

    @Override
    @NotNull
    public Collection<Task> findAllByProjectId(@Nullable String projectId, @Nullable String userId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId, userId);
    }

    @Nullable
    @Override
    public String findIdByName(@Nullable final String name, @Nullable final String userId) {
        if (name == null || name.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return taskRepository.findIdByName(name, userId);
    }


    @Override
    public void removeAllByProjectId(@Nullable final String projectId, @Nullable final String userId) {
        if (projectId == null || projectId.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        taskRepository.removeAllByProjectId(projectId, userId);
    }

    @Override
    public void removeAllProjectTask(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        taskRepository.removeAllProjectTask(userId);
    }

    @NotNull
    @Override
    public Collection<Task> searchByName(@Nullable final String userId, @Nullable final String string) {
        if (userId == null || userId.isEmpty()) return new ArrayList<>();
        if (string == null || string.isEmpty()) return new ArrayList<>();
        return taskRepository.searchByName(userId, string);
    }

    @NotNull
    @Override
    public Collection<Task> searchByDescription(@Nullable final String userId, @Nullable final String string) {
        if (userId == null || userId.isEmpty()) return new ArrayList<>();
        if (string == null || string.isEmpty()) return new ArrayList<>();
        return taskRepository.searchByDescription(userId, string);
    }

    @NotNull
    @Override
    public Collection<Task> sortAllTask(
            @Nullable final String userId,
            @Nullable String selectedSort
    ) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (selectedSort == null || selectedSort.isEmpty()) return findAll(userId);

        return taskRepository.findAll(userId, getComparator(selectedSort));
    }

    @NotNull
    private Comparator<Task> getComparator(@Nullable String selectedSort) {
        @NotNull Comparator sortOrder = Comparator.naturalOrder();
        if (selectedSort.toUpperCase().contains(Sort.SUFFIX)) {
            sortOrder = Comparator.reverseOrder();
            selectedSort = selectedSort.substring(0, selectedSort.length() - Sort.SUFFIX.length());
        }
        @NotNull Comparator sortOptions = Comparator.nullsLast(sortOrder);
        if (selectedSort.toUpperCase().contains(Sort.PREFIX)) {
            sortOptions = Comparator.nullsFirst(sortOrder);
            selectedSort = selectedSort.substring(Sort.PREFIX.length());
        }
        switch (selectedSort.toLowerCase()) {
            case "start date":
                return Comparator.comparing(Task::getStartDate, sortOptions);
            case "finish date":
                return Comparator.comparing(Task::getFinishDate, sortOptions);
            case "status":
                return Comparator.comparing(Task::getStatus, sortOptions);
            default:
                throw new IllegalSortTypeException("cannot sort by type :" + selectedSort);
        }
    }

}