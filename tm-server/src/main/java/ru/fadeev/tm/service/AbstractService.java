package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.IService;
import ru.fadeev.tm.entity.AbstractEntity;
import ru.fadeev.tm.api.repository.IRepository;

import java.util.LinkedList;
import java.util.List;

public class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    final IRepository<E> repository;

    public AbstractService(@NotNull final IRepository<E> repository) {
        this.repository = repository;
    }

    @NotNull
    @Override
    public List<E> findAll() {
        return new LinkedList<>(repository.findAll());
    }

    @Nullable
    @Override
    public E findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return repository.findOne(id);
    }

    @Nullable
    @Override
    public E remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return repository.remove(id);
    }

    @Nullable
    @Override
    public E persist(@Nullable final E e) {
        if (e == null) return null;
        return repository.persist(e);
    }

    @Nullable
    @Override
    public E merge(@Nullable final E e) {
        if (e == null) return null;
        return repository.merge(e);
    }

    @Override
    public void removeAll() {
        repository.removeAll();
    }

}