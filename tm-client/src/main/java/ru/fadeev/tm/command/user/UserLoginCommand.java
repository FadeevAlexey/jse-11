package ru.fadeev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.IUserEndpoint;
import ru.fadeev.tm.api.endpoint.Role;
import ru.fadeev.tm.api.endpoint.Session;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.IllegalUserNameException;
import ru.fadeev.tm.exception.IllegalUserPasswordException;
import ru.fadeev.tm.exception.InvalidSessionException;

public final class UserLoginCommand extends AbstractCommand {

    @Override
    public boolean permission(Role role) {
        return true;
    }

    @NotNull
    @Override
    public String getName() {
        return "user-login";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "log in task manager";
    }

    @Override
    public void execute() {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @NotNull final IUserEndpoint userService = serviceLocator.getUserEndpoint();
        terminal.println("[USER LOGIN]");
        terminal.println("ENTER LOGIN");
        @Nullable final String login = terminal.readString();
        @NotNull final boolean loginExist = userService.isLoginExistUser(login);
        if (!loginExist) throw new IllegalUserNameException("Can't find user");
        terminal.println("ENTER PASSWORD");
        @Nullable final String password = terminal.readString();
        if (password == null) throw new IllegalUserPasswordException("Illegal password");
        Session session = serviceLocator.getSessionEndpoint().openSession(login,password);
        if (session == null) throw new InvalidSessionException("Invalid session");
        serviceLocator.getAppStateService().setSession(session);
        terminal.println("[OK]\n");
    }

}