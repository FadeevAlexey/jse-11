package ru.fadeev.tm.endpoint;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.ITaskEndpoint;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.Session;
import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@Setter
@WebService(endpointInterface = "ru.fadeev.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint() {
        super(null);
    }

    public TaskEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @WebMethod
    public List<Task> findAllTaskAdmin(@WebParam(name = "session") @Nullable final Session session) {
        serviceLocator.getSessionService().checkSession(session, Role.ADMINISTRATOR);
        return serviceLocator.getTaskService().findAll();
    }

    @Override
    @Nullable
    @WebMethod
    public Task findOneTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) {
        serviceLocator.getSessionService().checkSession(session, Role.ADMINISTRATOR);
        return serviceLocator.getTaskService().findOne(id);
    }

    @Override
    @Nullable
    @WebMethod
    public Task removeTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) {
        serviceLocator.getSessionService().checkSession(session, Role.ADMINISTRATOR);
        return serviceLocator.getTaskService().remove(id);
    }

    @Override
    @Nullable
    @WebMethod
    public Task persistTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "task") @Nullable final Task task
    ) {
        serviceLocator.getSessionService().checkSession(session);
        return serviceLocator.getTaskService().persist(task);
    }

    @Override
    @Nullable
    @WebMethod
    public Task mergeTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "task") @Nullable final Task task
    ) {
        serviceLocator.getSessionService().checkSession(session);
        return serviceLocator.getTaskService().merge(task);
    }

    @Override
    @WebMethod
    public void removeAllTaskAdmin(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().checkSession(session, Role.ADMINISTRATOR);
        serviceLocator.getTaskService().removeAll();
    }

    @Override
    @Nullable
    @WebMethod
    public String findIdByNameTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable final String name
    ) {
        serviceLocator.getSessionService().checkSession(session);
        return serviceLocator.getTaskService().findIdByName(name, session.getUserId());
    }

    @Override
    @NotNull
    @WebMethod
    public Collection<Task> findAllTask(@WebParam(name = "session") @Nullable final Session session) {
        serviceLocator.getSessionService().checkSession(session);
        return serviceLocator.getTaskService().findAll(session.getUserId());
    }

    @Override
    @WebMethod
    public void removeAllTask(
            @WebParam(name = "session") @Nullable final Session session) {
        serviceLocator.getSessionService().checkSession(session);
        serviceLocator.getTaskService().removeAll(session.getUserId());
    }

    @Override
    @NotNull
    public Collection<Task> sortAllTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "selectedSort") final @Nullable String selectedSort) {
        serviceLocator.getSessionService().checkSession(session);
        return serviceLocator.getTaskService().sortAllTask(session.getUserId(), selectedSort);
    }

    @Override
    @NotNull
    @WebMethod
    public Collection<Task> searchByNameTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "string") @Nullable final String string
    ) {
        serviceLocator.getSessionService().checkSession(session);
        return serviceLocator.getTaskService().searchByName(session.getUserId(), string);
    }

    @Override
    @NotNull
    @WebMethod
    public Collection<Task> searchByDescriptionTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "string") @Nullable final String string
    ) {
        serviceLocator.getSessionService().checkSession(session);
        return serviceLocator.getTaskService().searchByDescription(session.getUserId(), string);
    }

    @Override
    @NotNull
    @WebMethod
    public Collection<Task> findAllByProjectIdTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) {
        serviceLocator.getSessionService().checkSession(session);
        return serviceLocator.getTaskService().findAllByProjectId(projectId, session.getUserId());
    }

    @Override
    @WebMethod
    public void removeAllByProjectIdTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) {
        serviceLocator.getSessionService().checkSession(session);
        serviceLocator.getTaskService().removeAllByProjectId(projectId, session.getUserId());
    }

    @Override
    @WebMethod
    public void removeAllProjectTask(@WebParam(name = "session") @Nullable final Session session) {
        serviceLocator.getSessionService().checkSession(session);
        serviceLocator.getTaskService().removeAllProjectTask(session.getUserId());
    }

}