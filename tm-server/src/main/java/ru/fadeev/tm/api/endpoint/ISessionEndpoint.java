package ru.fadeev.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ISessionEndpoint {

    @Nullable
    @WebMethod
    Session openSession(
            @WebParam(name = "session") @Nullable String login,
            @WebParam(name = "password") @Nullable String password);

    @WebMethod
    void closeSession(@WebParam(name = "session") @Nullable Session session);

}