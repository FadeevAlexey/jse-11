package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Task;

import java.util.Collection;

public interface ITaskService extends IService<Task> {

    @Nullable
    String findIdByName(@Nullable String name, @Nullable String userId);

    @NotNull
    Collection<Task> findAll(@Nullable String userId);

    void removeAll(@Nullable String userId);

    @NotNull
    Collection<Task> sortAllTask(@NotNull String userId, @Nullable String selectedSort);

    @NotNull
    Collection<Task> searchByName(@Nullable String userId, @Nullable String string);

    @NotNull
    Collection<Task> searchByDescription(@Nullable String userId, @Nullable String string);

    @NotNull
    Collection<Task> findAllByProjectId(@Nullable String projectId, @Nullable String userId);

    void removeAllByProjectId(@Nullable String projectId, @Nullable String userId);

    void removeAllProjectTask(@Nullable String userId);

}