package ru.fadeev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.repository.IRepository;
import ru.fadeev.tm.entity.AbstractEntity;
import ru.fadeev.tm.exception.EntityDuplicateException;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    final Map<String, E> entities = new LinkedHashMap<>();

    @NotNull
    @Override
    public List<E> findAll() {
        return new LinkedList<>(entities.values());
    }

    @NotNull
    @Override
    public E findOne(@NotNull final String id) {
        return entities.get(id);
    }

    @NotNull
    @Override
    public E remove(@NotNull final String id) {
        return entities.remove(id);
    }

    @Nullable
    @Override
    public E persist(@NotNull final E e) {
        if (entities.containsKey(e.getId())) throw new EntityDuplicateException();
        return entities.put(e.getId(), e);
    }

    @Nullable
    @Override
    public E merge(@NotNull final E e) {
        return entities.put(e.getId(), e);
    }

    @Override
    public void removeAll() {
        entities.clear();
    }

}