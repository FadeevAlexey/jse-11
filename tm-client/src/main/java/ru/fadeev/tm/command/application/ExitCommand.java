package ru.fadeev.tm.command.application;

import org.jetbrains.annotations.NotNull;
import ru.fadeev.tm.api.endpoint.Role;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;

public final class ExitCommand extends AbstractCommand {

    @Override
    public boolean permission(Role role) {
        return true;
    }

    @NotNull
    @Override
    public String getName() {
        return "exit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Exit from the program.";
    }

    @Override
    public void execute() {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        terminal.println("Thank you for using Task Manager. See you soon.");
    }

}