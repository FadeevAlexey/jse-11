package ru.fadeev.tm.command.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.Role;
import ru.fadeev.tm.api.endpoint.Session;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;

import java.nio.file.AccessDeniedException;

public final class DataXmlJaxbLoadFile extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "data-xml-jaxb-load";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "load data form XML format file (JAXB)";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @Nullable final Session session = serviceLocator.getAppStateService().getSession();
        if (session == null) throw new AccessDeniedException("Access Denied");
        terminal.println("[LOAD DATA FROM XML FORMAT FILE (JAXB)]");
        serviceLocator.getDomainEndpoint().loadFromXmlJaxb(session);
        serviceLocator.getAppStateService().setSession(null);
        terminal.println("You will be logged out of the application");
        terminal.println("[OK]\n");
    }

    @Override
    public @Nullable Role[] accessRole() {
        return new Role[]{Role.ADMINISTRATOR};
    }

}