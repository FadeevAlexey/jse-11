package ru.fadeev.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Session;
import ru.fadeev.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IUserEndpoint {

    @WebMethod
    List<User> findAllUser(@WebParam(name = "session") @Nullable Session session);

    @WebMethod
    @Nullable User findOneUser(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "id") @Nullable String id
    );

    @WebMethod
    @Nullable User removeUser(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "id") @Nullable String id
    );

    @WebMethod
    @Nullable User persistUser(
            @WebParam(name = "user") @Nullable User user
    );

    @WebMethod
    @Nullable User mergeUser(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user
    );

    @WebMethod
    void removeAllUser(
            @WebParam(name = "session") @Nullable Session session
    );

    @WebMethod
    boolean isLoginExistUser(
            @WebParam(name = "login") @Nullable String login);

    @Nullable
    @WebMethod
    User findUserByLoginUser(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "login") @Nullable String login
    );

    void setAdminRole (
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user
    );

}