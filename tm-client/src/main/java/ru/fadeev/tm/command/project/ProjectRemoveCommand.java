package ru.fadeev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.IProjectEndpoint;
import ru.fadeev.tm.api.endpoint.Session;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.AccessDeniedException;
import ru.fadeev.tm.exception.IllegalProjectNameException;

public final class ProjectRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-remove";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove selected project.";
    }

    @Override
    public void execute() {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @NotNull final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        @Nullable final Session session = serviceLocator.getAppStateService().getSession();
        if(session == null) throw new AccessDeniedException("Access denied");
        terminal.println("[PROJECT REMOVE]\nENTER NAME");
        @Nullable final String projectId
                = projectEndpoint.findIdByNameProject(session, terminal.readString());
        if (projectId == null) throw new IllegalProjectNameException("Can't find project");
        projectEndpoint.removeProject(session, projectId);
        serviceLocator.getTaskEndpoint().removeAllByProjectIdTask(session, projectId);
        terminal.println("[OK]\n");
    }

}