package ru.fadeev.tm.context;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.*;
import ru.fadeev.tm.api.repository.*;
import ru.fadeev.tm.api.service.*;
import ru.fadeev.tm.endpoint.*;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;
import ru.fadeev.tm.repository.*;
import ru.fadeev.tm.service.*;

import javax.xml.ws.Endpoint;

public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(userService, taskService, projectService);

    @Getter
    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository, userRepository, propertyService);

    @Getter
    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @Getter
    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @Getter
    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @Getter
    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    @Getter
    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);


    public void init() throws Exception {
        propertyService.init();
        initUser();
        start();
    }

    private void registry(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String link = String.format(
                "http://%s:%s/%s?wsdl", propertyService.getServerHost(), propertyService.getServerPort(), name);
        System.out.println(link);
        Endpoint.publish(link, endpoint);
    }

    public void start() {
        registry(domainEndpoint);
        registry(userEndpoint);
        registry(taskEndpoint);
        registry(projectEndpoint);
        registry(sessionEndpoint);
    }


    private void initUser() {
        @NotNull final User user = new User();
        user.setName("User");
        user.setPassword("user");
        @NotNull final User admin = new User();
        admin.setName("Admin");
        admin.setPassword("admin");
        admin.setRole(Role.ADMINISTRATOR);
        userService.persist(user);
        userService.persist(admin);
    }

}