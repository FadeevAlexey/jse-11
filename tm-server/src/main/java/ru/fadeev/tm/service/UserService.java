package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.IUserService;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;
import ru.fadeev.tm.api.repository.IUserRepository;
import ru.fadeev.tm.util.PasswordHashUtil;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    @Nullable
    @Override
    public User persist(@Nullable User user) {
        if (user == null) return null;
        user.setPassword(PasswordHashUtil.md5(user.getPassword()));
        return super.persist(user);
    }

    @Nullable
    @Override
    public User merge(@Nullable User user) {
        if (user == null) return null;
        user.setPassword(PasswordHashUtil.md5(user.getPassword()));
        return super.persist(user);
    }

    public UserService(@NotNull IUserRepository userRepository) {
       super(userRepository);
       this.userRepository = userRepository;
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) {
        if (login == null) return false;
        if (login.isEmpty()) return false;
        return userRepository.isLoginExist(login);
    }

    @Nullable
    @Override
    public User findUserByLogin(@Nullable final String login){
        if (login == null || login.isEmpty()) return null;
        return userRepository.findUserByLogin(login);
    }

    @Override
    public void setAdminRole(@Nullable User user) {
        if (user == null) return;
        user.setRole(Role.ADMINISTRATOR);
        persist(user);
    }

}