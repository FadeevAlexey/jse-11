package ru.fadeev.tm.entity;

import lombok.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.enumerated.Status;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@ToString(callSuper=true)
public final class Project extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 5371138000748416222L;

    @Nullable
    private String name = "";

    @Nullable
    private String description = "";

    @Nullable
    private Date startDate;

    @Nullable
    private Date finishDate;

    @Nullable
    private String userId;

    @NotNull
    private Status status = Status.PLANNED;

}