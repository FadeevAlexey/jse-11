package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Session;
import ru.fadeev.tm.enumerated.Role;

public interface ISessionService extends IService<Session> {

    @Nullable
    Session openSession(@Nullable String login, @Nullable String password);

    void closeSession(@Nullable Session session);

    boolean contains(@Nullable String sessionId);

    void checkSession(@Nullable Session currentSession);

    void checkSession(@Nullable Session currentSession, @NotNull final Role role);

}