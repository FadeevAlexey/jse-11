package ru.fadeev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.fadeev.tm.entity.Session;

public interface ISessionRepository extends IRepository<Session> {

    Session findByUserId(@NotNull String userId);

    void removeSessionBySignature(@NotNull String signature);

    boolean contains(@NotNull String sessionId);
}