package ru.fadeev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.Session;
import ru.fadeev.tm.command.AbstractCommand;

public final class UserLogoutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-logout";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "logout from task-manager";
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getAppStateService().getSession();
        serviceLocator.getSessionEndpoint().closeSession(session);
        serviceLocator.getAppStateService().setSession(null);
        serviceLocator.getTerminalService().println("[OK]\n");
    }

}