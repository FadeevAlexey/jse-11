package ru.fadeev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.IUserEndpoint;
import ru.fadeev.tm.api.endpoint.Role;
import ru.fadeev.tm.api.endpoint.Session;
import ru.fadeev.tm.api.endpoint.User;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.AccessDeniedException;
import ru.fadeev.tm.exception.IllegalUserNameException;

public final class UserCreateCommand extends AbstractCommand {

    @Override
    public boolean permission(Role role) {
        return true;
    }

    @NotNull
    @Override
    public String getName() {
        return "user-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "creates a new user account";
    }

    @Override
    public void execute() {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @Nullable final Session session = serviceLocator.getAppStateService().getSession();
        if (session == null) throw new AccessDeniedException("Access denied");
        @Nullable final IUserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
        @Nullable final boolean isAdministrator = Role.ADMINISTRATOR == session.getRole();
        terminal.println("[CREATE ACCOUNT]");
        terminal.println("ENTER NAME");
        @Nullable final String login = terminal.readString();
        @NotNull boolean isLoginExist = userEndpoint.isLoginExistUser(login);
        if (login == null || login.isEmpty()) throw new IllegalUserNameException("Incorrect name");
        if (isLoginExist) throw new IllegalUserNameException("User with same name already exist");
        @NotNull User user = new User();
        user.setName(login);
        terminal.println("Enter password:");
        user.setPassword(terminal.readString());
        if (!isAdministrator) userEndpoint.persistUser(user);
        setRole(session, user);
        terminal.println("[OK]\n");
    }

    public void setRole(@NotNull final Session session, @NotNull User user) {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        terminal.println("would you like give new account administrator rights? y/n");
        if ("y".equals(terminal.readString())) serviceLocator.getUserEndpoint().setAdminRole(session, user);
        else serviceLocator.getUserEndpoint().persistUser(user);
    }

}