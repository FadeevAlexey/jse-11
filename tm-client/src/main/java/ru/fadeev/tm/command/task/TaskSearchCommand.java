package ru.fadeev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.ITaskEndpoint;
import ru.fadeev.tm.api.endpoint.Session;
import ru.fadeev.tm.api.endpoint.Task;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.AccessDeniedException;
import ru.fadeev.tm.exception.IllegalSearchRequestException;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public final class TaskSearchCommand extends AbstractCommand {

    @NotNull
    @Override
    public final String getName() {
        return "task-search";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Search for tasks by NAME or DESCRIPTION.";
    }

    @Override
    public void execute() {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @Nullable final Session session = serviceLocator.getAppStateService().getSession();
        if (session == null || session.getUserId() == null) throw new AccessDeniedException("Access denied");
        terminal.println("[TASK SEARCH]");
        terminal.println("SEARCH REQUEST");
        @Nullable final String searchRequest = terminal.readString();
        if (searchRequest == null || searchRequest.isEmpty())
            throw new IllegalSearchRequestException("Search request can't be empty");
        terminal.println("Select search type: NAME, DESCRIPTION, ALL");
        @Nullable final String typeSearch = terminal.readString();
        if (typeSearch == null || typeSearch.isEmpty())
            throw new IllegalSearchRequestException("Search request can't be empty");
        terminal.printTaskList(getTaskList(session, searchRequest, typeSearch));
    }

    @NotNull
    private Collection<Task> getTaskList(
            @NotNull final Session session,
            @NotNull final String searchRequest,
            @NotNull final String typeSearch
    ) {
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        switch (typeSearch.toLowerCase()) {
            case "name": return taskEndpoint.searchByNameTask(session, searchRequest);
            case "description": return taskEndpoint.searchByDescriptionTask(session, searchRequest);
            case "all": {
                @NotNull final Collection<Task> searchByName =
                        taskEndpoint.searchByNameTask(session, searchRequest);
                @NotNull final Collection<Task> searchByDescription =
                        taskEndpoint.searchByDescriptionTask(session, searchRequest);
                @NotNull final Set<Task> allResult = new HashSet<>();
                allResult.addAll(searchByName);
                allResult.addAll(searchByDescription);
                return allResult;
            }
            default: throw new IllegalSearchRequestException("invalid search type");
        }
    }

}