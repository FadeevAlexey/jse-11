package ru.fadeev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.IProjectEndpoint;
import ru.fadeev.tm.api.endpoint.ITaskEndpoint;
import ru.fadeev.tm.api.endpoint.Session;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.AccessDeniedException;
import ru.fadeev.tm.exception.IllegalProjectNameException;

public final class ProjectTasksCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-tasks";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all tasks inside project.";
    }

    @Override
    public void execute() {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @NotNull final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        @Nullable final Session session = serviceLocator.getAppStateService().getSession();
        if (session == null || session.getUserId() == null) throw new AccessDeniedException("Access denied");
        @Nullable final String currentUserId = session.getUserId();
        terminal.println("[PROJECT TASKS]");
        terminal.println("ENTER PROJECT NAME");
        @Nullable final String projectId
                = projectEndpoint.findIdByNameProject(session, terminal.readString());
        if (projectId == null) throw new IllegalProjectNameException("Can't find project");
        terminal.printTaskList(taskEndpoint.findAllByProjectIdTask(session, projectId));
    }

}