package ru.fadeev.tm.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.RequiredArgsConstructor;
import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.eclipse.persistence.jaxb.JAXBContextProperties;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.IDomainService;
import ru.fadeev.tm.api.service.IProjectService;
import ru.fadeev.tm.api.service.ITaskService;
import ru.fadeev.tm.api.service.IUserService;
import ru.fadeev.tm.constant.Data;
import ru.fadeev.tm.entity.Domain;
import ru.fadeev.tm.entity.Project;
import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.exception.DomainException;

import javax.naming.spi.ObjectFactory;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.file.Files;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@RequiredArgsConstructor
public final class DomainService implements IDomainService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final IProjectService projectService;

    @Override
    public void saveData(@Nullable final Domain domain) {
        if (domain == null) throw new DomainException("Domain can't be empty");
        domain.setUsers(userService.findAll());
        domain.setTasks(taskService.findAll());
        domain.setProjects(projectService.findAll());
    }

    @Override
    public void loadData(@Nullable final Domain domain) {
        if (domain == null) throw new DomainException("Domain can't be empty");
        if (domain.getUsers() == null) domain.setUsers(Collections.emptyList());
        if (domain.getProjects() == null) domain.setProjects(Collections.emptyList());
        if (domain.getTasks() == null) domain.setTasks(Collections.emptyList());
        for (User user : domain.getUsers()) userService.persist(user);
        for (Task task : domain.getTasks()) taskService.persist(task);
        for (Project project : domain.getProjects()) projectService.persist(project);
    }


    public void saveToBinary() throws Exception {
        if (!Files.exists(Data.DIRECTORY)) Files.createDirectory(Data.DIRECTORY);
        @NotNull final Domain domain = new Domain();
        saveData(domain);
        try (
                @NotNull final FileOutputStream fileOutput = new FileOutputStream(Data.BINARY.toFile());
                @NotNull final ObjectOutputStream outputStream = new ObjectOutputStream(fileOutput);
        ) {
            outputStream.writeObject(domain);
        }
    }

    public void loadFromBinary() throws Exception {
        if (!Files.exists(Data.BINARY)) throw new FileNotFoundException("File can't find");
        try (
                @NotNull final FileInputStream fileInputStream = new FileInputStream(Data.BINARY.toFile());
                @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        ) {
            @NotNull final Domain domain = (Domain) objectInputStream.readObject();
            clearAll();
            loadData(domain);
        }
    }

    public void saveToJsonFasterXml() throws Exception {
        if (!Files.exists(Data.DIRECTORY)) Files.createDirectory(Data.DIRECTORY);
        @NotNull final Domain domain = new Domain();
        saveData(domain);
        @NotNull final ObjectWriter writer = new ObjectMapper().writerWithDefaultPrettyPrinter();
        writer.writeValue(Data.JSON_FASTERXML.toFile(), domain);

    }

    public void loadFromJsonFasterXml() throws Exception {
        if (!Files.exists(Data.JSON_FASTERXML)) throw new FileNotFoundException("File can't find");
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        @NotNull final Domain domain = mapper.readValue(Data.JSON_FASTERXML.toFile(), Domain.class);
        clearAll();
        loadData(domain);

    }

    public void saveToJsonJaxb() throws Exception {
        if (!Files.exists(Data.DIRECTORY)) Files.createDirectory(Data.DIRECTORY);
        @NotNull final Domain domain = new Domain();
        saveData(domain);
        @NotNull final JAXBContext context = JAXBContextFactory.createContext(
                new Class[]{Domain.class, ObjectFactory.class},
                getProperties()
        );
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        marshaller.marshal(domain, Data.JSON_JAXB.toFile());
    }

    public void loadFromJsonJaxb() throws Exception {
        if (!Files.exists(Data.JSON_JAXB)) throw new FileNotFoundException("File can't find");
        @NotNull final JAXBContext context = JAXBContextFactory.createContext(
                new Class[]{Domain.class, ObjectFactory.class},
                getProperties()
        );
        @NotNull final Unmarshaller unMarshaller = context.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unMarshaller.unmarshal(Data.JSON_JAXB.toFile());
        clearAll();
        loadData(domain);
    }

    public void saveToXmlJaxb() throws Exception {
        if (!Files.exists(Data.XML_JAXB)) Files.createDirectory(Data.XML_JAXB);
        @NotNull final Domain domain = new Domain();
        saveData(domain);
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(domain, Data.XML_JAXB.toFile());
    }

    public void loadFromXmlJaxb() throws Exception {
        if (!Files.exists(Data.XML_JAXB)) throw new FileNotFoundException("File can't find");
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(Data.XML_JAXB.toFile());
        clearAll();
        loadData(domain);
    }

    public void saveToXmlFasterxml() throws Exception {
        if (!Files.exists(Data.XML_FASTERXML)) Files.createDirectory(Data.XML_FASTERXML);
        @NotNull final Domain domain = new Domain();
        saveData(domain);
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final ObjectWriter writer = xmlMapper.writerWithDefaultPrettyPrinter();
        writer.writeValue(Data.XML_FASTERXML.toFile(), domain);
    }

    public void loadFromXmlFasterxml() throws Exception {
        if (!Files.exists(Data.XML_FASTERXML)) throw new FileNotFoundException("File can't find");
        @NotNull final XmlMapper mapper = new XmlMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        @NotNull final Domain domain = mapper.readValue(Data.XML_FASTERXML.toFile(), Domain.class);
        clearAll();
        loadData(domain);
    }

    private void clearAll() {
        taskService.removeAll();
        projectService.removeAll();
        userService.removeAll();
    }

    @NotNull
    private Map<String, Object> getProperties() {
        @NotNull final Map<String, Object> properties = new HashMap<>();
        properties.put(JAXBContextProperties.MEDIA_TYPE, "application/json");
        properties.put(JAXBContextProperties.JSON_INCLUDE_ROOT, false);
        return properties;
    }

}