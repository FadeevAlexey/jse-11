package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.Role;
import ru.fadeev.tm.api.endpoint.Session;
import ru.fadeev.tm.api.repository.IAppStateRepository;
import ru.fadeev.tm.api.service.IAppStateService;
import ru.fadeev.tm.command.AbstractCommand;

import java.util.*;

public class AppStateService  implements IAppStateService {

    private final IAppStateRepository appStateRepository;

    public AppStateService(IAppStateRepository appStateRepository) {
        this.appStateRepository = appStateRepository;
    }

    @Override
    @Nullable
    public Role getRole() {
        return appStateRepository.getRole();
    }

    @Override
    public void putCommand(@Nullable final String description, @Nullable final AbstractCommand abstractCommand) {
        if (description == null || description.isEmpty()) return;
        if (abstractCommand == null) return;
        appStateRepository.putCommand(description, abstractCommand);
    }

    @Override
    @Nullable
    public AbstractCommand getCommand(@Nullable final String command) {
        if (command == null || command.isEmpty()) return null;
        return appStateRepository.getCommand(command);
    }

    @Override
    @NotNull
    public List<AbstractCommand> getCommands() {
        return appStateRepository.getCommands();
    }

    @Override
    public boolean hasPermission(@Nullable Role... roles) {
        if (roles == null) return false;
        return appStateRepository.hasPermission();
    }

    @Override
    public @Nullable Session getSession() {
        return appStateRepository.getSession();
    }

    @Override
    public void setSession(@Nullable Session session) {
        appStateRepository.setSession(session);
    }

}