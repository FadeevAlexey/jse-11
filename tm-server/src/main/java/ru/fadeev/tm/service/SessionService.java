package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.repository.ISessionRepository;
import ru.fadeev.tm.api.repository.IUserRepository;
import ru.fadeev.tm.api.service.IPropertyService;
import ru.fadeev.tm.api.service.ISessionService;
import ru.fadeev.tm.entity.Session;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;
import ru.fadeev.tm.exception.AccessDeniedException;
import ru.fadeev.tm.exception.InvalidSessionException;
import ru.fadeev.tm.util.PasswordHashUtil;
import ru.fadeev.tm.util.SignatureUtil;


import java.util.Date;

public class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull final ISessionRepository sessionRepository;
    @NotNull final IUserRepository userRepository;
    @NotNull final IPropertyService propertyService;

    public SessionService(
            @NotNull final ISessionRepository sessionRepository ,
            @NotNull final IUserRepository userRepository,
            @NotNull final IPropertyService propertyService
    ) {
        super(sessionRepository);
        this.sessionRepository = sessionRepository;
        this.userRepository = userRepository;
        this.propertyService = propertyService;
    }

    @Nullable
    @Override
    public Session openSession(@Nullable final String login, @Nullable String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        @Nullable final User user = userRepository.findUserByLogin(login);
        if (user == null) return null;
        password = PasswordHashUtil.md5(password);
        if (password == null) return null;
        if (!password.equals(user.getPassword())) return null;
        Session session = new ru.fadeev.tm.entity.Session();
        session.setUserId(user.getId());
        session.setRole(user.getRole());
        session.setSignature(SignatureUtil.sign(
                session, propertyService.getSessionSalt(),propertyService.getSessionCycle()));
        persist(session);
        return session;
    }

    @Override
    public void closeSession(@Nullable final ru.fadeev.tm.entity.Session session){
        if (session == null) return;
        if (session.getSignature() == null) return;
        sessionRepository.removeSessionBySignature(session.getSignature());
    }

    @Override
    public boolean contains(@Nullable String sessionId) {
        if (sessionId == null) return false;
        return sessionRepository.contains(sessionId);
    }

    public void checkSession(@Nullable final ru.fadeev.tm.entity.Session currentSession) {
        final long currentTime = new Date().getTime();
        if (currentSession == null) throw new InvalidSessionException("Invalid session");
        if (!contains(currentSession.getId())) throw new InvalidSessionException("Invalid session");
        if (currentSession.getUserId() == null) throw new InvalidSessionException("Invalid session");
        if (currentSession.getSignature() == null)throw new InvalidSessionException("Invalid session");
        if (currentSession.getRole() == null) throw new InvalidSessionException("Invalid session");
        @NotNull final ru.fadeev.tm.entity.Session session = new ru.fadeev.tm.entity.Session();
        session.setSignature(currentSession.getSignature());
        session.setRole(currentSession.getRole());
        session.setId(currentSession.getId());
        session.setCreationTime(currentSession.getCreationTime());
        session.setUserId(currentSession.getUserId());
        @Nullable final String sessionSignature =
                SignatureUtil.sign(session, propertyService.getSessionSalt(), propertyService.getSessionCycle());
        @Nullable final String currentSessionSignature =
                SignatureUtil.sign(currentSession, propertyService.getSessionSalt(), propertyService.getSessionCycle());
        if (sessionSignature == null || currentSessionSignature == null) throw new InvalidSessionException("Invalid session");
        if (! sessionSignature.equals(currentSessionSignature)) throw new InvalidSessionException("Invalid session");;
        if (currentSession.getCreationTime() - currentTime > propertyService.getSessionLifetime())
            throw new InvalidSessionException("Invalid session");
    }

    public void checkSession(@Nullable final ru.fadeev.tm.entity.Session currentSession, @NotNull final Role role) {
        if (currentSession == null) throw new InvalidSessionException("Invalid session");
        checkSession(currentSession);
        if (currentSession.getRole() != role) throw new AccessDeniedException("Access denied");
    }

}