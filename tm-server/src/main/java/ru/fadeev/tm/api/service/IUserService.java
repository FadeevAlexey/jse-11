package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.User;

public interface IUserService extends IService<User> {

    boolean isLoginExist(@Nullable String login);

    @Nullable
    User findUserByLogin(@Nullable String login);

    void setAdminRole (@Nullable User user);

}