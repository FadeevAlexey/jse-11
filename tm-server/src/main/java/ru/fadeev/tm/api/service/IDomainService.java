package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Domain;

public interface IDomainService {

    void saveData(@Nullable Domain domain);

    void loadData(@Nullable Domain domain);

    void saveToBinary() throws Exception;

    void loadFromBinary() throws Exception;

    void saveToJsonFasterXml() throws Exception;

    void loadFromJsonFasterXml() throws Exception;

    void saveToJsonJaxb() throws Exception;

    void loadFromJsonJaxb() throws Exception;

    void saveToXmlJaxb() throws Exception;

    void loadFromXmlJaxb() throws Exception;

    void saveToXmlFasterxml() throws Exception;

    void loadFromXmlFasterxml() throws Exception;

}