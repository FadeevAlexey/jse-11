package ru.fadeev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.repository.ISessionRepository;
import ru.fadeev.tm.entity.Session;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Nullable
    @Override
    public Session findByUserId(@NotNull final String userId){
        return findAll().stream()
                .filter(session -> session.getUserId().equals(userId))
                .findFirst().orElse(null);
    }

    @Override
    public void removeSessionBySignature(@NotNull final String signature){
        findAll().stream()
                .filter(session -> signature.equals(session.getSignature()))
                .forEach(session -> remove(session.getId()));
    }

    public boolean contains(@NotNull final String sessionId){
      return super.entities.containsKey(sessionId);
    }

}