package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface IPropertyService {

    void init() throws Exception;

    @Nullable
    String getServerPort();

    @Nullable
    String getServerHost();

    @Nullable
    String getSessionSalt();

    int getSessionCycle();

    int getSessionLifetime();

}
